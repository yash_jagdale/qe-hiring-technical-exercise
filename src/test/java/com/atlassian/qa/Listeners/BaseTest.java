package com.atlassian.qa.Listeners;

import com.atlassian.qa.config.TestDataHandler;
import com.atlassian.qa.driver.Browser;
import com.atlassian.qa.pages.Login;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.*;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Properties;

public class BaseTest {


    protected WebDriver browser;
    protected Properties testDataHandler;
    private final static Logger logger = Logger.getLogger(BaseTest.class);
    Login login;

    @BeforeSuite
    public void beforeSuite() {
        logger.info("Before Suite Execution Started");
        browser = Browser.getInstance();
    }


    @AfterSuite
    public void afterSuite() {
        logger.info("After Suite Execution Started");
        browser.quit();
    }


    @BeforeTest
    public void beforeTest() {
        logger.info("Before Test Execution Started");
        logger.info("Logging in to application");
        login = PageFactory.initElements(browser, Login.class);
        login.performGoogleLogin();
        logger.info("Logged in successfully");
    }

    @BeforeMethod
    public void beforeMethod(Method method) throws IOException {
        testDataHandler = new TestDataHandler(method.getName()).load();
    }

    @AfterTest
    public void logoutFromApp() {
        login.logout();
    }
}
