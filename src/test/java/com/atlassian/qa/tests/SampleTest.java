package com.atlassian.qa.tests;

import com.atlassian.qa.Listeners.BaseTest;
import com.atlassian.qa.helper.PageTemplates;
import com.atlassian.qa.pages.HomePage;
import com.atlassian.qa.pages.NewPage;
import com.atlassian.qa.pages.RestrictionDialogue;
import com.atlassian.qa.pages.ViewPage;
import org.testng.Assert;
import org.testng.annotations.Test;


public class SampleTest extends BaseTest {



    @Test(priority = 1)
    public void createNewPage() {
        HomePage homePage = new HomePage();
        NewPage newPage = homePage.createNewPage();
        newPage.createNewPage(PageTemplates.BLANK_PAGE);
        newPage.fillPageDetails(testDataHandler.getProperty("PAGE_TITLE"), testDataHandler.getProperty("PAGE_CONTENTS"));
        ViewPage viewPage = newPage.publishPage();
        Assert.assertEquals(viewPage.getTitle(), testDataHandler.getProperty("PAGE_TITLE"));
        Assert.assertEquals(viewPage.getContents(), testDataHandler.getProperty("PAGE_CONTENTS"));
    }

    @Test(priority = 2)
    public void changeRestrictions() {
        HomePage homePage = new HomePage();
        ViewPage viewPage = homePage.navigateToPage(testDataHandler.getProperty("PAGE_TITLE"));
        viewPage.openToolMenu();
        RestrictionDialogue restrictionDialogue = viewPage.clickRestrictionsMenu();
        restrictionDialogue.selectRestrictionType(testDataHandler.getProperty("PAGE_RESTRICTION"));
        restrictionDialogue.addUserOrGroup(testDataHandler.getProperty("USER"));
        restrictionDialogue.applyChanges();
    }

}
