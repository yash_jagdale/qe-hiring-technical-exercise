package com.atlassian.qa.config;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class TestDataHandler {

    private String testCaseName;
    private String testDataLocation = System.getProperty("user.dir") + "/src/test/resources/test-data/%s.data";
    private final static Logger logger = Logger.getLogger(TestDataHandler.class);

    public TestDataHandler(String testCaseName) {
        logger.info("Loading test data for " + testCaseName);
        this.testCaseName = testCaseName;
    }

    public Properties load() throws IOException {
        Properties pro = new Properties();
        pro.load(new FileInputStream(new File(String.format(testDataLocation, testCaseName))));
        return pro;
    }


}
