package com.atlassian.qa.config;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * This class provides instance of ConfigReader for reading values from "config.properties", <env>/env.config and ".log" files
 */
public class ConfigReader {

    private FileInputStream fis = null;
    private Properties pro = new Properties();
    private String runnerConfigFilePath = System.getProperty("user.dir") + "/src/test/resources/%s.config";
    private final static Logger logger = Logger.getLogger(ConfigReader.class);

    private static ConfigReader configReaderInstance = null;

    private ConfigReader() {
        loadProperties();
    }

    public static ConfigReader getInstance() {
        logger.debug("Fetching ConfigReader Instance");
        if (configReaderInstance == null)
            configReaderInstance = new ConfigReader();

        return configReaderInstance;
    }

    private void loadProperties() {
        logger.info("Loading Properties");
        if (System.getenv("env") != null) {
            runnerConfigFilePath = String.format(runnerConfigFilePath, System.getenv("env"));
        } else if (System.getProperty("env") != null) {
            runnerConfigFilePath = String.format(runnerConfigFilePath, System.getProperty("env"));
        } else {
            logger.info("No Env Specified selecting default env");
            runnerConfigFilePath = String.format(runnerConfigFilePath, "env");
        }
        try {
            fis = new FileInputStream(new File(runnerConfigFilePath));
        } catch (FileNotFoundException e) {
            logger.error("Specified env file does not existing in resources folder");
            e.printStackTrace();
        }
        try {
            pro.load(fis);
        } catch (IOException e) {
            logger.error("Error while loading properties");
            e.printStackTrace();
        }

    }


    public String getProperty(String key) {
        return pro.getProperty(key);
    }

}