package com.atlassian.qa.pages;

import com.atlassian.qa.driver.Browser;
import lombok.Getter;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.atlassian.qa.helper.ElementHelper.*;

@Getter
public class HomePage {

    private final static Logger logger = Logger.getLogger(HomePage.class);

    private WebDriver browser = Browser.getInstance();
    private By btnCreate = By.cssSelector("#createGlobalItem");
    private By btnSearch = By.id("quickSearchGlobalItem");
    private By inpSearch = By.cssSelector("[placeholder=\"Search Confluence\"]");
    private String pageLink = ".//span[contains(text(), '%s')]";


    public NewPage createNewPage() {
        logger.info("Navigating to New Page");
        click(btnCreate);
        return new NewPage();
    }


    public ViewPage navigateToPage(String pageTitle) {
        logger.info("navigateToPage to " + pageTitle);
        click(btnSearch);
        type(inpSearch, pageTitle);
        String identifier = String.format(pageLink, pageTitle);
        waitForElementToBeDisplayed(By.xpath(identifier));
        clickByAction(By.xpath(identifier));
        return new ViewPage();
    }
}
