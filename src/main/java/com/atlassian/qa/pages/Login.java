package com.atlassian.qa.pages;

import com.atlassian.qa.config.ConfigReader;
import com.atlassian.qa.driver.Browser;
import com.atlassian.qa.helper.WaitUtils;
import lombok.Getter;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.atlassian.qa.helper.ElementHelper.*;


@Getter
public class Login {

    private final static Logger logger = Logger.getLogger(Login.class);


    private By inpUserName = By.id("username");
    private By inpPassword = By.id("password");;
    private By btnContinue = By.id("login-submit");
    private By btnGoogleSignIn = By.id("google-signin-button");
    private By inpGoogleUsername = By.id("identifierId");
    private By btnNext = By.id("identifierNext");
    private By inpGooglePassword = By.name("password");;
    private By btnPassNext = By.id("passwordNext");

    private By profileIcon = By.id("profileGlobalItem");
    private By logout = By.cssSelector("[href=\"/wiki/logout.action\"]");
    WaitUtils waitUtils =  new WaitUtils();


    public void performLogin(String username, String password) {
        logger.info("Performing login");
        sendKeys(this.inpUserName, username);
        click(btnContinue);
        sendKeys(this.inpPassword, password);
        click(btnContinue);

        waitUtils.waitUntilElementVisible(profileIcon);
        logger.info("User logged in");
    }

    public void performGoogleLogin() {
        String username = ConfigReader.getInstance().getProperty("USERNAME");
        String password = ConfigReader.getInstance().getProperty("PASSWORD");
        performLogin(username, password);
    }

    public void logout() {
        waitUtils.waitUntilElementVisible(profileIcon);
        click(profileIcon);
        clickByAction(logout);
    }
}
