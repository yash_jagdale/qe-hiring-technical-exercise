package com.atlassian.qa.pages;

import com.atlassian.qa.helper.WaitUtils;
import org.openqa.selenium.By;
import static com.atlassian.qa.helper.ElementHelper.*;

public class ViewPage {

    private By pageTitle = By.id("title-text");
    private By pageContents = By.id("content");
    private WaitUtils waitUtils = new WaitUtils();
    private By toolMenu = By.id("tools-menu-trigger");
    private By restrictionMenuItem = By.id("bm-restrictions-link");


    public ViewPage() {
        waitUtils.waitUntilElementVisible(pageTitle);
    }

    public String getTitle() {
        return waitUtils.waitUntilElementVisible(pageTitle).getText();
    }

    public String getContents() {
        return waitUtils.waitUntilElementVisible(pageContents).getText();
    }


    public void openToolMenu() {
        click(toolMenu);
    }

    public RestrictionDialogue clickRestrictionsMenu() {
        click(restrictionMenuItem);
        return new RestrictionDialogue();
    }
}
