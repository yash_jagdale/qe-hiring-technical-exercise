package com.atlassian.qa.pages;

import com.atlassian.qa.helper.WaitUtils;
import org.openqa.selenium.By;
import static com.atlassian.qa.helper.ElementHelper.*;
public class RestrictionDialogue {

    WaitUtils waitUtils = new WaitUtils();
    private By pageIdentifier = By.xpath(".//div[@role=\"dialog\"]//span[text()='Restrictions']");
    private By restrictions = By.cssSelector("[data-test-id=\"restrictions-dialog.content-mode-select\"]");
    private String restrictionOptions = "//span[text() = '%s']";
    private By userGroupSearchBox = By.cssSelector("[data-test-id = user-and-group-search] span");
    private By filteredUserList = By.cssSelector("[data-test-id=\"user-group-search-label\"]");
    private By apply = By.xpath(".//div[@role=\"dialog\"]//footer//button//span[text()='Apply']");
    private By addButton = By.cssSelector("[data-test-id=\"restrictions-dialog.users-and-groups-search\"] button");

    public RestrictionDialogue() {
        waitUtils.waitUntilElementVisible(pageIdentifier);
    }


    public String getCurrentRestrictions() {
        return waitUtils.waitUntilElementVisible(restrictions).getText();

    }

    public void selectRestrictionType(String editing_restricted) {
        By optionToSelect = By.xpath(String.format(restrictionOptions, editing_restricted));
        click(restrictions);
        waitUtils.waitUntilElementVisible(restrictions).findElement(optionToSelect).click();
    }

    public void addUserOrGroup(String userOrGroupName) {
        click(userGroupSearchBox);
        type(userGroupSearchBox, userOrGroupName);
        click(filteredUserList);
        click(addButton);
    }
    public void applyChanges() {
        click(apply);
    }
}
