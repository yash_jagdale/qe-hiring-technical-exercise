package com.atlassian.qa.pages;

import com.atlassian.qa.driver.Browser;
import com.atlassian.qa.helper.PageTemplates;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.atlassian.qa.helper.ElementHelper.*;
import static com.atlassian.qa.helper.ElementHelper.click;

public class NewPage {

    private final static Logger logger = Logger.getLogger(NewPage.class);

    private By btnCreateOnDialogue = By.cssSelector("[data-test-id=\"create-dialog-create-button\"]");
    private By inpPageTitle = By.cssSelector("[data-test-id=\"editor-title\"]");
    private By inpPageContents = By.cssSelector(".ak-editor-content-area p");
    private By btnCloseEditing = By.id("close-button");
    private By btnPublish = By.id("publish-button");


    private String template = "[data-item-module-complete-key=\"com.atlassian.confluence.plugins.confluence-create-content-plugin:%s\"]";


    public void createNewPage(PageTemplates pageTemplate) {
        logger.info("Creating new Page with Template " + pageTemplate);
        click(By.cssSelector(String.format(template, pageTemplate)));
        click(btnCreateOnDialogue);
        waitForElementToBeDisplayed(btnCloseEditing);
    }

    public void fillPageDetails(String pageTitle, String pageContents) {
        logger.info("Filling new page details");
        type(inpPageTitle, pageTitle);
        waitForElementToBeClickable(inpPageContents);
        type(inpPageContents, pageContents);
    }

    public ViewPage publishPage() {
        logger.info("publishing page");
        click(btnPublish);
        return new ViewPage();
    }


}
