package com.atlassian.qa.helper;

import lombok.Getter;

@Getter
public enum PageTemplates {

    BLANK_PAGE("BLANK_PAGE", "create-blank-page");


    private String templateName;
    private String templateId;

    PageTemplates(String templateName, String templateId) {
        this.templateName = templateName;
        this.templateId = templateId;
    }

    @Override
    public String toString() {
        return templateId;
    }
}
