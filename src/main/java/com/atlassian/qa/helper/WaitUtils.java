package com.atlassian.qa.helper;

import com.atlassian.qa.driver.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.*;

import java.time.Duration;

public class WaitUtils {

    Wait wait;
    private WebDriver webDriver = Browser.getInstance();

    public WaitUtils() {
        this.wait = new FluentWait(webDriver)
                .withTimeout(Duration.ofSeconds(60))
                .pollingEvery(Duration.ofSeconds(60))
                .ignoring(ElementNotInteractableException.class);
    }

    public WaitUtils(WebDriver webDriver, int timeout) {
        this.webDriver = webDriver;
        this.wait = new FluentWait(webDriver)
                .withTimeout(Duration.ofSeconds(timeout))
                .pollingEvery(Duration.ofSeconds(timeout))
                .ignoring(ElementNotInteractableException.class);
    }

    public WebElement waitUntilElementVisible(By element) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        return this.webDriver.findElement(element);
    }

    public WebElement waitUntilElementClickable(By element) {
        this.wait.until(ExpectedConditions.elementToBeClickable(element));
        return this.webDriver.findElement(element);
    }

}
