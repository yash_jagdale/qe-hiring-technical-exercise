package com.atlassian.qa.helper;

import com.atlassian.qa.driver.Browser;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class ElementHelper {
    private static WaitUtils waitUtils = new WaitUtils();
    private final static Logger logger = Logger.getLogger(ElementHelper.class);


    public static void sendKeys(By element, String text) {
        logger.debug(String.format("Send keys action on %s is started", element));
        waitUtils.waitUntilElementVisible(element).sendKeys(text);
        logger.debug(String.format("Send keys action on %s is completed", element));
    }

    public static void click(By element) {
        logger.debug(String.format("click action on %s is started", element));
        waitUtils.waitUntilElementVisible(element).click();
        logger.debug(String.format("click action on %s is completed", element));
    }

    public static void waitForElementToBeDisplayed(By element) {
        logger.debug(String.format("waitForElementToBeDisplayed action on %s is started", element));
        waitUtils.waitUntilElementVisible(element);
        logger.debug(String.format("waitForElementToBeDisplayed action on %s is completed", element));
    }

    public static void waitForElementToBeClickable(By element) {
        logger.debug(String.format("waitForElementToBeClickable action on %s is started", element));
        waitUtils.waitUntilElementClickable(element);
        logger.debug(String.format("waitForElementToBeClickable action on %s is completed", element));
    }

    public static void clickByAction(By element) {
        logger.debug(String.format("clickByActions action on %s is started", element));
        WebDriver browser = Browser.getInstance();
        Actions actions = new Actions(browser);
        WebElement webElement = waitUtils.waitUntilElementVisible(element);
        actions.moveToElement(webElement).click().build().perform();
        logger.debug(String.format("clickByAction action on %s is completed", element));
    }

    public static void type(By element, String text) {
        logger.debug(String.format("type action on %s is started", element));
        WebDriver browser = Browser.getInstance();
        Actions actions = new Actions(browser);
        WebElement webElement = waitUtils.waitUntilElementVisible(element);
        actions.sendKeys(webElement, text).perform();
        logger.debug(String.format("type action on %s is completed", element));
    }

}
