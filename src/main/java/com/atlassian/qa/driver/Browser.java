package com.atlassian.qa.driver;

import com.atlassian.qa.config.ConfigReader;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class Browser {
    private final static Logger logger = Logger.getLogger(Browser.class);

    private WebDriver driver = null;
    private static Browser browser = new Browser();


    /*
     * This method will help you to use self managed Driver instance
     * Just perform get instance
     */
    private Browser() {
    }

    public static WebDriver getInstance() {
        logger.debug("Fetching Instance of browser");
        if (browser.driver == null) {
            logger.info("No Existing browser created, Starting new Browser");
            browser.startSelfManagedDriver();
            setBaseFlow(browser.driver);
        }
        return browser.driver;
    }

    private static void setBaseFlow(WebDriver browser) {
        browser.get(ConfigReader.getInstance().getProperty("BASE_URL"));
        if (ConfigReader.getInstance().getProperty("TIMEOUT") != null) {
            browser.manage().timeouts().implicitlyWait(Integer.parseInt(ConfigReader.getInstance().getProperty("TIMEOUT")), TimeUnit.SECONDS);
        }
        browser.manage().window().maximize();
    }


    /*
     * Method will create Self managed webdriver as performed getInstance
     * But webdriver instance was empty
     * provide your WebDriver instance to method
     *
     */
    private void startSelfManagedDriver() {
        logger.info("Starting Self Managed WebDriver Instance");
        String hubUrl = ConfigReader.getInstance().getProperty("HUB_URL");
        if (hubUrl != null && !hubUrl.equals(""))
            this.driver = new HubBasedBrowser(hubUrl).create();
        else
            this.driver = new StandaloneDriver().create();
    }

}
