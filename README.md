Selenium Automation
===================

Software Requirement:
```
java - 1.8 or above
maven
testng
```


```
Folder Structure >
-
>    src/main/java - This will contains all the code related to framework except test cases and some listeners  
- config: 
    This will hold all the config readers and data provider to the test cases
- driver:
    This will hold all browser handling
- helper: This will contains all the helper function(like wait and elements)
- pages: all the logical grouping of application into pages

>   src/test/java
- Listeners - This will hold all the actions which we need to perform before and after test cases or suites
- test cases - This will hold actual test cases(testng test cases)

> src/test/resources 
- test-data - This will hold test data needed for specific test case.
- testCases - This will hold logical grouping of test classes to testng xml

env.config - This contains all the env related properties
```

How to Run Tests:
=
```
mvn -DTestSuite=createPage.test.xml clean install 
```
